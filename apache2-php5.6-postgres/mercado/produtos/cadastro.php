<?php
	session_start();
	require_once '../config/conectaBanco.php';

	$nome = $_POST['nome'];
    $id_tipo_produto = $_POST['id_tipo_produto'];
    $valor = $_POST['valor'];
    $valor = str_replace(',', '.' , $valor);

    $sql_insere = "INSERT INTO produtos(id_tipo_produto, nome, valor)
    VALUES(?, ?, ?)";

    $stmt = $conn->prepare($sql_insere);

    $stmt->execute([$id_tipo_produto, $nome, $valor]);
	
   
	if($stmt->rowCount() > 0){ 
		$_SESSION['msg'] = "<div class='alert alert-success' role='alert'>Produto cadastrado com sucesso
		<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</spam></button>
		</div>";
		header("Location: index.php");
	}else{
		$_SESSION['msg'] = "<div class='alert alert-danger' role='alert'>Houve algum problema no cadastro
		<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</spam></button>
		</div>";
		header("Location: index.php");
	}