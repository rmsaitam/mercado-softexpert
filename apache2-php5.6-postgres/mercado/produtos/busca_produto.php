<?php require_once '../config/conectaBanco.php';

    $stmt = $conn->prepare("SELECT id, nome, id_tipo_produto FROM produtos WHERE id_tipo_produto = :id_tipo_produto");		
    $id_tipo_produto = $_REQUEST['id_tipo_produto'];
	$stmt->bindParam(':id_tipo_produto', $id_tipo_produto, PDO::PARAM_INT);	
    $stmt->execute();
    $produtos = $stmt->fetchALL(PDO::FETCH_ASSOC);
    foreach($produtos as $produto){
        $prod[] = array(
			'id'	=> $produto['id'],
			'nome' => utf8_encode($produto['nome']),
		);
    }

    echo json_encode($prod);