<?php require_once '../config/conectaBanco.php';

    $stmt = $conn->prepare("SELECT valor, imposto FROM produtos p
        INNER JOIN tipo_produto tp 
        ON (p.id_tipo_produto = tp.id)
        WHERE p.id = :id");		
    $id_produto = $_POST['id_produto'];
	$stmt->bindParam(':id', $id_produto, PDO::PARAM_INT);	
    $stmt->execute();
    $produtos = $stmt->fetchALL(PDO::FETCH_ASSOC);
    foreach($produtos as $produto){
        $prod[] = array(
			'valor'	=> $produto['valor'],
			'imposto' => utf8_encode($produto['imposto']),
		);
    }

    echo json_encode($prod);