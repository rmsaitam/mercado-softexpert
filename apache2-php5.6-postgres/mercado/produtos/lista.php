<?php require_once '../config/conectaBanco.php';
?> 
    <table class="table table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>Nome</th>
          <th>Preço </th>
        </tr>
      </thead>
      <tbody>
          <?php
                $stmt = $conn->prepare("SELECT p.id, p.nome, p.valor FROM produtos p
                INNER JOIN tipo_produto tp ON (p.id_tipo_produto = tp.id) ORDER BY p.nome");		
                $stmt->execute();
                $produtos = $stmt->fetchALL(PDO::FETCH_ASSOC);
                foreach($produtos as $produto){
                    echo "<tr><td>" .$produto['id']. "</td>
                        <td>"  .$produto['nome']. "</td>
                        <td>"  .$produto['valor']. "</tr>";
                }
            ?>
      </tbody>
    </table

    