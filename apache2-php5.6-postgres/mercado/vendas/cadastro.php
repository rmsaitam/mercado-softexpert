<?php
	session_start();
	require_once '../config/conectaBanco.php';

    $id_produto = $_POST['id_produto'];
    $qtde = $_POST['qtde'];
	$valor_total = $_POST['valor_total'];
	$valor_total_imposto = $_POST['valor_total_imposto'];

    $sql_insere = "INSERT INTO venda_produto(id_produto, qtde, valor_total, valor_total_imposto)
    VALUES(?, ?, ?, ?)";

    $stmt = $conn->prepare($sql_insere);

    $stmt->execute([$id_produto, $qtde, $valor_total, $valor_total_imposto]);
	
   
	if($stmt->rowCount() > 0){ 
		$_SESSION['msg'] = "<div class='alert alert-success' role='alert'>Venda cadastrada com sucesso
		<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</spam></button>
		</div>";
		header("Location: index.php");
	}else{
		$_SESSION['msg'] = "<div class='alert alert-danger' role='alert'>Houve algum problema no cadastro
		<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</spam></button>
		</div>";
		header("Location: index.php");
	}