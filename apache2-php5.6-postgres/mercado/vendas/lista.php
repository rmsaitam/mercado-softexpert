<?php require_once '../config/conectaBanco.php';
?> 
    <table class="table table-hover">
      <thead>
        <tr>
          <th>ID</th>
          <th>Tipo produto</th>
          <th>Produto</th>
          <th>Quantidade</th>
          <th>Valor unitário </th>
          <th>Valor Imposto </th>
          <th>Valor Total </th>
          <th>Valor Total com imposto </th>
        </tr>
      </thead>
      <tbody>
          <?php
                $stmt = $conn->prepare("
                SELECT pv.id, tp.nome as tp_produto, p.nome as produto, pv.qtde, p.valor, tp.imposto, 
                pv.valor_total, pv.valor_total_imposto FROM produtos p
                INNER JOIN tipo_produto tp ON (p.id_tipo_produto = tp.id) 
                INNER JOIN venda_produto pv ON (pv.id_produto = p.id)
                ORDER BY p.nome");		
                $stmt->execute();
                $vendas_produtos = $stmt->fetchALL(PDO::FETCH_ASSOC);
                foreach($vendas_produtos as $venda_produto){
                    echo "<tr><td>" .$venda_produto['id']. "</td>
                        <td>"  .$venda_produto['tp_produto']. "</td>
                        <td>"  .$venda_produto['produto']. "</td>
                        <td>"  .$venda_produto['qtde']. "</td>
                        <td>"  .$venda_produto['valor']. "</td>
                        <td>"  .$venda_produto['imposto']. "</td>
                        <td>"  .$venda_produto['valor_total']. "</td>
                        <td>"  .$venda_produto['valor_total_imposto']. "</td></tr>";
                }
            ?>
      </tbody>
    </table

    