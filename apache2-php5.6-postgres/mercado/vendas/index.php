<?php
session_start();
require_once "../config/conectaBanco.php"; 
require_once "../menu.php";
    if(!empty($_SESSION['msg'])){
        echo $_SESSION['msg'];
        unset($_SESSION['msg']);
    }
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Mercado - Vendas</title>

        <!-- Bootstrap -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">
        <style type="text/css">
            .carregando{
                color:#ff0000;
                display:none;
            }
        </style>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
  <body>
    <h1>Vendas</h1>

    <div class="container theme-showcase" role="main">
        <form class="form-horizontal" method="POST" action="cadastro.php">
            <div class="form-group">
                <label class="col-sm-4 control-label">Tipo do produto:</label>
                <div class="col-sm-4">
                    <select class="form-control" name="id_tipo_produto" id="id_tipo_produto">
                        <option value="">Escolha o tipo do produto</option>
                        <?php
                            $stmt = $conn->prepare("SELECT * FROM tipo_produto ORDER BY nome");
                            $stmt->execute();
                            $tipo_produtos = $stmt->fetchALL(PDO::FETCH_ASSOC);
                            foreach ($tipo_produtos as $tp_prod){
                                echo '<option value="'.$tp_prod['id'].'">'.$tp_prod['nome'].'</option>';
                            }
                        ?>
                    </select>
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-4 control-label">Produto:</label>
                <div class="col-sm-4">
                    <span class="carregando">Aguarde, carregando...</span>					
                    <select class="form-control" name="id_produto" id="id_produto">
                        <option value="">Escolha o produto</option>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">Quantidade: </label>
                <div class="col-sm-4">
                    <input type="text"  class="form-control" name="qtde" id="qtde"
                    onchange='calculaValorTotal()'>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label"> Valor Total: </label>
                <div class="col-sm-4">
                    <input type="text"  class="form-control" data-symbol="R$ " data-thousands="." data-decimal="," name="valor_total" id="valor_total" readonly>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-4 control-label">Valor Total com Imposto: </label>
                <div class="col-sm-4">
                    <input type="text"  class="form-control" data-symbol="R$ " data-thousands="." data-decimal="." name="valor_total_imposto" id="valor_total_imposto" readonly
                    value="">
                </div>
            </div>
            
            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <input type="submit"  class="btn btn-primary" value="Cadastrar">
                </div>
            </div>
        </form>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../js/jquery.min.js"></script>
    <script src="../js/jquery.maskMoney.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/busca_produto.js"></script>
    <script src="../js/helpers.js"></script>

    <?php require_once "lista.php"; ?>
  </body>
</html>