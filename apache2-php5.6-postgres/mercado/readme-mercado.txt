Extrair o arquivo Softexpert-mercado-Reginaldo.tar.gz e mover o diretório mercado para o diretório raiz do Apache

Linux: 
tar -xzvf Softexpert-mercado-Reginaldo.tar.gz 
cd Softexpert-mercado-Reginaldo
cp -r mercado /var/www/html

Windows:
copiar o diretório mercado para c:/xampp/htdocs

criar o banco de dados chamado mercado e o usuário com os privilégios: create, drop, alter, select, insert, update, delete

Criar base de dados e usuário
postgres# CREATE DATABASE mercado;
postgres# CREATE USER <user> WITH PASSWORD 'pass';

Aplicar GRANT ao usuário
GRANT CONNECT ON DATABASE mercado TO <user>;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO <user>;
GRANT ALL PRIVILEGES ON DATABASE mercado to <user>;

No arquivo phinx.yml adicionar o username, password, database na diretiva development

Estando com o banco de dados criado e o usuário com os privilégios adequados, pode executar o comando seguinte, que irá criar as tabelas do banco de dados mercado
vendor/bin/phinx migrate

No arquivo config/config.php, adicionar as credencias de acesso ao banco de dados

Criar um Vhost no Apache
Linux
No arquivo /etc/apache2/sites-available/mercado.conf
Deve conter o contéudo 
<VirtualHost *:80>
    #ServerAdmin root@localhost
    ServerName mercado.local
    DocumentRoot "/var/www/html/mercado"
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
    <Directory "/var/www/html/mercado">
	DirectoryIndex index.php index.html index.htm
	AllowOverride All
	Order allow,deny
	Allow from all
    </Directory>
</VirtualHost>

Habilitar o Vhost: # a2ensite mercado.conf

Reload do Apache: # systemctl reload apache2

Windows com o XAMPP instalado e adicionado o módulo PHP-PGSQL
No arquivo C:\xampp\apache\conf\extra\httpd-vhosts.conf
Deve conter o conteúdo
<VirtualHost *:80>
    ServerName mercado.local
    DocumentRoot "C:/xampp/htdocs/mercado"
    ErrorLog ${APACHE_LOG_DIR}/error.log
    CustomLog ${APACHE_LOG_DIR}/access.log combined
    <Directory "C:/xampp/htdocs/mercado">
        DirectoryIndex index.php index.html index.htm
        AllowOverride All
        Order allow,deny
        Allow from all
    </Directory>
</VirtualHost>

Reinicie o Apache pelo XAMPP

Adicionar o Vhost no arquivo hosts
Linux
Adicionar no arquivo /etc/hosts o Vhost
127.0.0.1	mercado.local

Windows
Editar o arquivo C:\Windows\System32\drivers\etc\hosts como Administrador
127.0.0.1       mercado.local

No browser, acesse na URL http://mercado.local



