<?php require_once '../config/conectaBanco.php';
require_once '../menu.php';
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Mercado - Vendas</title>

        <!-- Bootstrap -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">
    </head>
  <body>
    <h1>Dashboard</h1>

    <div class="container theme-showcase" role="main">

        <?php 
            $stmt = $conn->prepare("
            SELECT MAX(valor_total) as maior_valor_venda,
            MIN(valor_total) as menor_valor_venda,
            MAX(valor_total_imposto) as maior_valor_imposto,
            MIN(valor_total_imposto) as menor_valor_imposto,
            SUM(valor_total) total_valor_vendido,
            SUM(qtde) total_qtde 
            FROM venda_produto");		
            $stmt->execute();
            $venda_produto = $stmt->fetchALL(PDO::FETCH_ASSOC);

            if($venda_produto[0]['maior_valor_venda'] != NULL &&
            $venda_produto[0]['menor_valor_venda'] != NULL &&
            $venda_produto[0]['maior_valor_imposto'] != NULL &&
            $venda_produto[0]['menor_valor_imposto'] != NULL &&
            $venda_produto[0]['total_valor_vendido'] != NULL &&
            $venda_produto[0]['total_qtde'] != NULL) { ?>

            <table class="table table-hover">
            <thead>
                <tr>
                <th>Mais vendido R$</th>
                <th>Menos vendido R$</th>
                <th>Maior valor com imposto  R$</th>
                <th>Menor valor com imposto R$</th>
                <th>Valor total R$ </th>
                <th>Quantidade </th>
                </tr>
            </thead>
            <tbody>
                
                <?php
                echo "<tr><td> " .$venda_produto[0]['maior_valor_venda'] . "</td>";
                echo "<td> " .$venda_produto[0]['menor_valor_venda'] . "</td>";
                echo "<td> " .$venda_produto[0]['maior_valor_imposto'] . "</td>";
                echo "<td> " .$venda_produto[0]['menor_valor_imposto'] . "</td>"; 
                echo "<td> " .$venda_produto[0]['total_valor_vendido'] . "</td>";
                echo "<td>" .$venda_produto[0]['total_qtde'] . "</td></tr>";

            }
            else {
                echo "<tr><td> Ainda não há vendas</td> </tr>";
            }

        ?>
    </div>
  </body>
</html>

 