 
<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Mercado</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    </head>
    <body>
        <h1>Mercado</h1>

        <div class="container theme-showcase" role="main">

            <a href="../dashboard"> Home </a> &nbsp &nbsp &nbsp
            <a href="../tipo_produto"> Cadastrar tipo produto </a> &nbsp &nbsp &nbsp
            <a href="../produtos"> Cadastrar produto </a>  &nbsp &nbsp &nbsp
            <a href="../vendas"> Vendas </a> &nbsp &nbsp &nbsp
        </div>
    </body>
</html>
 

