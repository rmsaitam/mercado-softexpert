$(function(){
    $('#id_tipo_produto').change(function(){
        if( $(this).val() ) {
            $('#id_produto').hide();
            $('.carregando').show();
            $.getJSON('../produtos/busca_produto.php?search=',
                {id_tipo_produto: $(this).val(), ajax: 'true'}, function(j){
                var options = '<option value="">Escolha Produto</option>';	
                for (var i = 0; i < j.length; i++) {
                    options += '<option value="' + j[i].id + '">' + j[i].nome + '</option>';
                }	
                $('#id_produto').html(options).show();
                $('.carregando').hide();
            });
        } else {
            $('#id_produto').html('<option value="">– Escolha Produto –</option>');
        }
    });
});