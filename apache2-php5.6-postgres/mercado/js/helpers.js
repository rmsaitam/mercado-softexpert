$(function() {
    $('#valor').maskMoney();
    $('#valor_total').maskMoney();
    $('#imposto').maskMoney();
    $('#valor_total_imposto').maskMoney();
});

function calculaValorTotal() {
    var id_produto = $('#id_produto').val();
    var qtde = $('#qtde').val();
    $.ajax({
        method: "POST",
        url: '../produtos/consulta_valor_produto.php',
        data: "id_produto="+id_produto
    }).done(function (data) {
        console.log(data);
        var obj = JSON.parse(data);
        $('#valor_total').val(obj[0].valor * qtde);
        $('#valor_total_imposto').val(parseFloat(obj[0].imposto) + parseFloat((qtde*obj[0].valor)));
    });
};
