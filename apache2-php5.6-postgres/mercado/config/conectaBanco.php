<?php
 
require_once 'config.php';
 
$dsn = "pgsql:host=" .HOST. ";port=".PORT.";dbname=" .DATABASE.";user=".USERNAME. ";password=" .PASS;

try{
 $conn = new PDO($dsn);
 
 if($conn){
   //echo "Connected to the <strong>" .DATABASE. "</strong> database successfully!";
 }
}catch (PDOException $e){
 
 echo $e->getMessage();
}
