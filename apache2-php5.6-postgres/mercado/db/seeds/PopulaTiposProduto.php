<?php


use Phinx\Seed\AbstractSeed;

class PopulaCategorias extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $tipos_produto = array(
                'ALIMENTOS',
                'BEBIDAS',
                'HIGIENE',
                'LIMPEZA',
                'FRUTAS, VERDURAS E LEGUMES',
                'PAPELARIA');

        foreach($tipos_produto as $tp) {
            $dados = array(
                array("nome" => $tp)
            );
            $tp = $this->table("tipos_produto");
            $tp->insert($dados)
            ->save();
        }
    }
}
