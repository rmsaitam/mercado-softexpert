<?php


use Phinx\Migration\AbstractMigration;

class TabelaProdutos extends AbstractMigration
{
    public function up()
    {
        if(!$this->hasTable('produtos')) {
            $table = $this->table('produtos');
            $table 
                ->addColumn('id_tipo_produto', 'integer', ['comment' => 'FK tipo_produto'])
                ->addColumn('nome', 'string', ['limit' => 255])
                ->addColumn('valor', 'decimal', ['precision'=>10, 'scale'=>2])
                ->addForeignKey('id_tipo_produto', 'tipo_produto', 'id', ['delete' => 'RESTRICT','update' => 'RESTRICT'])
                ->save();
        }
    }
    public function down()
    {
        if($this->hasTable('produtos')) {
            $this->dropTable('produtos');
        }
    }
}
