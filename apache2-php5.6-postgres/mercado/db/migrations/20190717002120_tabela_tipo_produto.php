<?php


use Phinx\Migration\AbstractMigration;

class TabelaTipoProduto extends AbstractMigration
{
    public function up()
    {   
        if(!$this->hasTable('tipo_produto')) {
            $table = $this->table('tipo_produto');
            $table 
                ->addColumn('nome', 'string', ['limit' => 255])
                ->addColumn('imposto', 'decimal', ['precision'=>10, 'scale'=>2])
                ->save();
        }
    }
    public function down()
    {
        if($this->hasTable('tipo_produto')){
            $this->dropTable('tipo_produto');
        }
    }
}
