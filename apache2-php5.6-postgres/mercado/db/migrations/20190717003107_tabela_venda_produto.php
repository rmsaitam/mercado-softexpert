<?php


use Phinx\Migration\AbstractMigration;

class TabelaVendaProduto extends AbstractMigration
{
    public function up()
    {
        if(!$this->hasTable('venda_produto')) {
            $table = $this->table('venda_produto');
            $table  
                ->addColumn('id_produto', 'integer', ['comment' => 'FK produtos'])
                ->addColumn('qtde', 'integer')
                ->addColumn('valor_total', 'decimal', ['precision'=>10, 'scale'=>2])
                ->addColumn('valor_total_imposto', 'decimal', ['precision'=>10, 'scale'=>2])
                ->addForeignKey('id_produto', 'produtos', 'id', ['delete' => 'RESTRICT','update' => 'RESTRICT'])
                ->save();
        }
    }
    public function down()
    {
        if($this->hasTable('venda_produto')) {
            $this->dropTable('venda_produto');
        }
    }
}
