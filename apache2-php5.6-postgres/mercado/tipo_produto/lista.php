<?php require_once '../config/conectaBanco.php';
?> 

    <table class="table table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nome</th>
                <th>Imposto </th>
            </tr>
        </thead>
        <tbody>
            <?php
                    
                    $stmt = $conn->prepare("SELECT id, nome, imposto FROM tipo_produto ORDER BY nome");		
                    $stmt->execute();
                    $tipos_produtos = $stmt->fetchALL(PDO::FETCH_ASSOC);
                
                    foreach($tipos_produtos as $tp_produto){
                        echo "<tr><td>" .$tp_produto['id']. "</td>
                            <td>"  .$tp_produto['nome']. "</td>
                            <td>"  .$tp_produto['imposto']. "</tr>";
                    }
            ?>
        </tbody>
    </table

    