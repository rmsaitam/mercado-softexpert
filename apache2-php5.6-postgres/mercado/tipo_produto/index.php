<?php 
session_start();
require_once "../config/conectaBanco.php"; 
require_once "../menu.php";
    if(!empty($_SESSION['msg'])){
        echo $_SESSION['msg'];
        unset($_SESSION['msg']);
    }
?>

<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Mercado - Vendas</title>

        <!-- Bootstrap -->
        <link href="../css/bootstrap.min.css" rel="stylesheet">
    </head>
  <body>
  <h1>Tipos de produto</h1>

    <div class="container theme-showcase" role="main">
        <form class="form-horizontal" method="POST" action="cadastro.php">
            <div class="form-group">
                <label class="col-sm-2 control-label">Nome: </label>
                <div class="col-sm-4">
                    <input type="text"  class="form-control" name="nome" id="nome">
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label"> Valor imposto: </label>
                <div class="col-sm-4">
                    <input type="text"  class="form-control" data-symbol="R$ " data-thousands="." data-decimal="," name="imposto" id="imposto">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <input type="submit"  class="btn btn-primary" value="Cadastrar">
                </div>
            </div>
        </form>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../js/jquery.min.js"></script>
    <script src="../js/jquery.maskMoney.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="../js/bootstrap.min.js"></script>
    <script src="../js/helpers.js"></script>
    <?php require_once "lista.php" ?>
  </body>
</html>