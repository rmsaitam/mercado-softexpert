<?php
	session_start();
	require_once $_SERVER['DOCUMENT_ROOT'] . '/config/conectaBanco.php';

	$nome = $_POST['nome'];
    $imposto = $_POST['imposto'];
	$imposto = str_replace(',', '.' , $imposto);
    error_log("imposto = " .$imposto);

    $sql_insere = "INSERT INTO tipo_produto(nome, imposto)
    VALUES(?, ?)";

    $stmt = $conn->prepare($sql_insere);

    $stmt->execute([$nome, $imposto]);
	
   
	if($stmt->rowCount() > 0){ 
		$_SESSION['msg'] = "<div class='alert alert-success' role='alert'>Tipo do produto cadastrado com sucesso
		<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</spam></button>
		</div>";
		header("Location: index.php");
	}else{
		$_SESSION['msg'] = "<div class='alert alert-danger' role='alert'>Houve algum problema no cadastro
		<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</spam></button>
		</div>";
		header("Location: index.php");
	}