# mercado-softexpert

Premissas: Ter o Docker e Docker-compose instalados

Ambiente Apache2 + PHP 5.6 + Postgres 9.4 no Docker

Clonar o repositório

`git clone https://gitlab.com/rmsaitam/mercado-softexpert.git`

Acessar o diretório mercado-softexpert/apache2-php56-postgres

`cd mercado-softexpert/apache2-php56-postgres`

Build e subir a aplicação

```
docker-compose build
docker-compose up -d
```

Acessar o container para executar o composer e migrations da aplicação

```
docker exec -it mercado-softexpert bash
root@827f988cb7a9:/var/www/html# composer install
root@827f988cb7a9:/var/www/html# vendor/bin/phinx migrate
```
Após os passos executados corretamente, no browser http://localhost:8080 

Feito!
